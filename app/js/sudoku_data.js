/*
 * This file holds model information tied to the Sudoku game board.
 * Events on the model are dispatched on the document, which can be subscribed to.
 * Any changes to the model should be done here.
 * This file also handles external API communications (getting a new board, getting a solution, etc).
 */

define(['require', 'postal', 'underscore', 'jquery'], function (require, postal, _, $) {
  
  var _channel  = postal.channel(),
      _board    = [],// Array of objects holding cell value, cell solution value, row num, col num, grid num
      _baseURL  = 'http://nameless-woodland-1994.herokuapp.com'; //'http://localhost:3000';
  
  /*
   * The _board array's elements are objects:
   * {
   *   rowNum: 0,
   *   colNum: 0,
   *   gridNum: 0,
   *   currentVal: 0,
   *   solutionVal: 2
   * }
   */


  // ----------------------------------
  // PUBLIC FUNCTIONS
  // ----------------------------------
  
  function getBoard () {
    return _.map(_board, function (element) {
      return element.currentVal;
    });
  }
  
  function getBoardAsString () {
    return getBoard().join('');
  }
  
  function getSolution () {
    return _.map(_board, function (element) {
      return element.solutionVal;
    });
  }
  
  function getSolutionAsString () {
    return getSolution().join('');
  }
  
  function getValAtIndex (index) {
    return _board[index];
  }
  
  function getPossibleValsForCell (cellnum) {
    var grid = getGridForCell(cellnum, true),
        col = getColumnForCell(cellnum, true),
        row = getRowForCell(cellnum, true),
        merged = grid.concat(col, row),
        sorted = merged.sort(function(a,b){return a-b;});
    
    return _.difference([1,2,3,4,5,6,7,8,9], sorted);
  }
  
  function updateValAtIndex (val, index, delay) {
    _board[index].currentVal = val;
    _channel.publish('board.currentVal', {val: val, index: index, delay: delay });
  }
  
  function updateSolutionValAtIndex (val, index) {
    _board[index].solutionVal = val;
    _channel.publish('board.solutionVal', {val: val, index: index});
  }
  
  function loadBoard () {
    console.log('Loading board...');
    $.ajax({
      url: _baseURL + '/api/puzzle',
      dataType: 'json',
      success: function (data) {
        var vals = data.puzzle;
        _board = [];
        _.each(vals, function (element, index) {
          _board.push({
            currentVal: (element == null ? 0 : element + 1),
            solutionVal: data.solved[index] + 1
          });
        });
        _channel.publish('boardLoaded');
      },
      error: function (error) {
        console.log('Error loading board');
        _channel.publish('boardLoadError');
      }
    });
  }

  /*
   * Returns the objects (or current value) of the board at the specified grid.
   * @param cellnum {Number} The cell index for which grid you want.
   * @param valOnly {Boolean} Whether to return an array of objects, or array of cell values.
   * @returns {Array} Either cell values or objects.
   */
  function getGridForCell (cellnum, valOnly) {
    // Get the grid number from the object at cellnum position
    var gridNum = _board[cellnum].gridNum;
    var gridVals = _.filter(_board, function (element) {
      return element.gridNum == gridNum;
    });
    if (!valOnly) {
      return gridVals;
    } else {
      return _.map(gridVals, function (element) {
        return element.currentVal;
      });
    }
  }

  function getRowForCell (cellnum, valOnly) {
    var rowNum = _board[cellnum].rowNum;
    var rowVals = _.filter(_board, function (element) {
      return element.rowNum == rowNum;
    });
    if (!valOnly) {
      return rowVals;
    } else {
      return _.map(rowVals, function (element) {
        return element.currentVal;
      });
    }
  }

  function getColumnForCell (cellnum, valOnly) {
    var colNum = _board[cellnum].colNum;
    var colVals = _.filter(_board, function (element) {
      return element.colNum == colNum;
    });
    if (!valOnly) {
      return colVals;
    } else {
      return _.map(colVals, function (element) {
        return element.currentVal;
      });
    }
  }
  
  function checkBoardForWin () {
    var win = checkBoard();
    console.log('Win? ' + win);
    switch (win) {
      case 'win':
        _channel.publish('boardSolved');
        break;
      case 'nowin':
        _channel.publish('boardNotSolved');
        break;
      case 'solutionNotReady':
        _channel.publish('solutionNotReady');
        break;
    }
  }
  
  /*
   * Since this module doesn't do any DOM, 
   * inject the data attributes (which are on the DOM)
   * @param data {array} Array of objects with the following signature:
   * {
   *   rowNum: 0,
   *   colNum: 0,
   *   gridNum: 0,
   *   currentVal: 0,
   *   solutionVal: 2
   * }
   */
  function injectDataAttributes (data) {
    _board = data;
  }
  
  
  // ----------------------------------
  // PRIVATE FUNCTIONS
  // ----------------------------------

  /*
   * Checks board for solved puzzle.
   * @param {Array} board: the one-dimensional board array
   * @returns {String} 'win', 'nowin'
   */
  function checkBoard () {

    var boardVals = getBoard(),
        solutionVals = getSolution(),
        diff = _.difference(boardVals, solutionVals);

    // Simple check first for any zeros
    if (_.contains(boardVals, 0)) {
      console.log('contains 0');
      return 'nowin';
    }
    if (diff.length == 0) {
      console.log('win');
      return 'win';
    }
    return 'nowin';
  }
  
  
  // ----------------------------------
  // EXPORT PUBLIC FUNCTIONS
  // ----------------------------------
  
  return {
    loadBoard                :loadBoard,
    getBoard                 :getBoard,
    getBoardAsString         :getBoardAsString,
    getSolution              :getSolution,
    getSolutionAsString      :getSolutionAsString,
    getGridForCell           :getGridForCell,
    getColumnForCell         :getColumnForCell,
    getRowForCell            :getRowForCell,
    getValAtIndex            :getValAtIndex,
    getPossibleValsForCell   :getPossibleValsForCell,
    updateValAtIndex         :updateValAtIndex,
    updateSolutionValAtIndex :updateSolutionValAtIndex,
    injectDataAttributes     :injectDataAttributes,
    checkBoardForWin         :checkBoardForWin
  }
});
