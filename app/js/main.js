define(['require', 'postal', 'jquery', 'FastClick', 'sudokuUI', 'panelRevealer'], function (require, postal, $, FastClick, sudokuUI, panelRevealer) {
  
  function init () {

    FastClick.attach(document.body);
    
    var channel = postal.channel();
    channel.subscribe('didClickStart', function (event) {
      sudokuUI.loadPuzzle();
    });

    channel.subscribe('boardLoaded', function (event) {
      panelRevealer.open();
    });
    
    channel.subscribe('revealStartOpen', function (event) {
      sudokuUI.zoomOut();
    });

    channel.subscribe('boardSolved', function (event) {
      // Wait a second
      setTimeout(function () {
        panelRevealer.close();
        panelRevealer.updateMessage('GAME SOLVED. PLAY AGAIN?');
      }, 1000);
    });

    sudokuUI.init('#js-sudoku-board');
    panelRevealer.init('#js-intro-revealer');
  }
  
  $(document).ready(init);
  
});