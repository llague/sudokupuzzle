requirejs.config({
  baseUrl: 'bower_components',
  paths: {
    'js': '../js',
    'jquery': 'jquery/dist/jquery',
    'underscore': 'underscore/underscore',
    'conduitjs': 'conduitjs/lib/conduit',
    'lodash': 'lodash/dist/lodash',
    'postal': 'postal.js/lib/postal',
    'jquery.gsap': 'gsap/src/uncompressed/jquery.gsap',
    'TweenLite': 'gsap/src/uncompressed/TweenLite',
    'TweenMax': 'gsap/src/uncompressed/TweenMax',
    'FastClick': 'fastclick/lib/fastclick',
    'sudokuData': '../js/sudoku_data',
    'sudokuUI': '../js/sudoku_ui',
    'panelRevealer': '../js/panel_revealer'
  },
  shim: {
    'jquery': {
      exports: '$'
    },
    'underscore': {
      exports: '_'
    },
    'TweenLite': {
      exports: 'TweenLite'
    },
    'TweenMax': {
      exports: 'TweenMax'
    },
    'FastClick': {
      exports: 'FastClick'
    }
  }
});

requirejs(['js/main']);