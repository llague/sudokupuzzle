define(['require', 'postal', 'jquery', 'TweenMax'], function (require, postal, $, TweenMax) {
  
  var _transforms          = [],
      $baseDiv             = null,
      $win                 = null,
      $startButton         = null,
      $headline            = null,
      $subhead             = null,
      _open                = false,
      _channel             = postal.channel(),
      _numPanels           = 4,
      _numPanelsDone       = 0;
      //'revealStartOpen''revealEndOpen''revealStartClose''revealEndClose'
  
  
  // ----------------------------------
  // PUBLIC FUNCTIONS
  // ----------------------------------
  
  function init (domObj) {
    $win = $(window);
    $baseDiv = $(domObj);
    $startButton = $baseDiv.find('#js-revealer-start-button');
    $headline = $baseDiv.find('h1').eq(0);
    $subhead = $baseDiv.find('.message').eq(0);

    initEvents();
  }
  
  function open () {
    _open = true;
    _channel.publish('revealStartOpen');
    
    TweenMax.to($headline, 0.5, {opacity: 0, ease:Quad.easeIn});
    TweenMax.to($subhead, 0.5, {opacity: 0, ease:Quad.easeIn});
    TweenMax.to($startButton, 0.5, {rotationX:90, opacity: 0, ease: Quad.easeIn});
    
    $baseDiv.find('.revealer--quadrant').each(function (index, elem) {
      var transY = Math.floor(Math.random() * 40 + 140),
          delay = index * 0.2,
          speed = 0.8,
          ease = Quad.easeIn;
      
      TweenMax.to($(this), 0.8, {rotationY: 75, y: transY, opacity: 0, delay: delay, ease:ease, onComplete: handleTransitionEnd});
    });
  }
  
  function close () {
    _open = false;
    _channel.publish('revealStartClose', { });
    
    $baseDiv.removeClass('hide');

    $startButton.find('span').text('NEW GAME');
    $startButton.find('img').addClass('invisible');
    $startButton.css({width: '180px', 'margin-left':'-90px', 'text-align': 'center'});
    
    TweenMax.to($headline, 0.5, {opacity: 1, ease:Quad.easeOut});
    TweenMax.to($subhead, 0.5, {opacity: 1, ease:Quad.easeOut});
    TweenMax.to($startButton, 0.5, {rotationX: 0, opacity: 1, ease: Quad.easeOut});
    $baseDiv.find('.revealer--quadrant').each(function (index, elem) {
      TweenMax.to($(this), 0.8, {rotationY: 0, y: 0, opacity: 1, delay: 0, ease:Quad.easeOut, onComplete: handleTransitionEnd});
    });
  }
  
  function isOpen () {
    return _open;
  }
  
  function updateMessage (message) {
    $subhead.text(message);
  }


  // ----------------------------------
  // PRIVATE FUNCTIONS
  // ----------------------------------
  
  function handleTransitionEnd (event) {
    _numPanelsDone++;
    if (_numPanelsDone == _numPanels) {
      _numPanelsDone = 0;
      if (_open) {
        _channel.publish('revealEndOpen', { });
        $baseDiv.addClass('hide');
      } else {
        _channel.publish('revealEndClose', { });
      }
    }
  }
  
  function handleStartClick (event) {
    event.preventDefault();
    $startButton.find('span').text('LOADING BOARD...');
    $startButton.find('img').removeClass('invisible');
    TweenMax.to($startButton, 0.5, {width: '260px', marginLeft:'-130px', textAlign: 'left'});
    // Wait for the animation to finish
    setTimeout(function() {
      _channel.publish('didClickStart');
    }.bind(this), 1000);
  }
  
  function initEvents () {
    $startButton.on('click', handleStartClick);
  }
  
  return {
    init: init,
    open: open,
    close: close,
    isOpen: isOpen,
    updateMessage: updateMessage 
  }
  
});