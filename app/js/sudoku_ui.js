define(['require', 'postal', 'underscore', 'jquery', 'TweenMax', 'sudokuData'], function (require, postal, _, $, TweenMax, sudokuData) {
  
  var _channel        = postal.channel(),
      $board          = null,
      $table          = null,
      headerHeight    = null,
      borderWidth     = null,
      $boardnums      = [], // Store this jquery selector since we will iterate over it often
      $focusedCell    = null,
      $statusMessage  = null,
      $newGameButton  = null,
      $solveButton    = null;


  // ----------------------------------
  // PUBLIC FUNCTIONS
  // ----------------------------------
  
  function init (domNode) {
    $board = $(domNode);
    $boardnums = $board.find('.board-cell--input');
    $statusMessage = $board.find('#js-game-status');
    $newGameButton = $board.find('#js-new-game-button');
    $solveButton = $board.find('#js-solve-game-button');
    
    _channel.subscribe('boardLoaded', handleBoardLoaded);
    _channel.subscribe('boardLoadError', handleBoardLoadError);
    _channel.subscribe('boardSolved', handleBoardSolved);
    _channel.subscribe('boardNotSolved', handleBoardNotSolved);
    _channel.subscribe('board.currentVal', handleCurrentValChanged);
    
    initUI();
    initEvents();
  }
  
  function zoomOut () {
    $board.removeClass('zoomed');
  }

  function loadPuzzle () {
    sudokuData.loadBoard();
  }


  // ----------------------------------
  // PRIVATE FUNCTIONS
  // ----------------------------------

  function initUI () {
    // Since we don't know the browser width beforehand, 
    // and the cells need to be square, set the height dynamically
    // The game board is never even partially hidden
    
    $table = $board.find('table');
    headerHeight = $board.find('#js-sudoku-header').outerHeight();
    borderWidth = parseInt($board.find('#c0').closest('td').css('border-left-width').slice(0, -2));
    
  }

  function initEvents () {
    var $input = $board.find('.board-cell--input');
    $input.on('focus', handleCellFocus);
    $input.on('input', handleCellInput);
    $board.on('click', function (event) {
      event.preventDefault();
      var $targ = $(event.target);
      if ($targ.hasClass('board-cell--hint')) {
        handleHintClicked(event, $targ);
      } else if ($targ.is($newGameButton)) {
        handleNewGameClick(event);
      } else if ($targ.is($solveButton)) {
        handleSolveClick(event);
      }
    });

    $(window).on('keyup', handleKeyUp);
    $(window).on('resize orientationchange', handleResize).trigger('resize');
  }
  
  
  
  function handleBoardLoaded () {
    var board = sudokuData.getBoard(),
        solution = sudokuData.getSolution(),
        data = [];
    
    // Add values to UI
    _.each(board, function (element, index) {
      var $input = getInputAtIndex(index),
          $back = $input.next('.board-cell--back'),
          $hint = $input.prev('.board-cell--hint');

      $statusMessage.text('Solution loaded.');
      $back.text(solution[index]);
      $input.removeAttr('readonly');
      
      if (element !== 0) {
        $input.val(element);
        $input.attr('readonly', 'true');
      } else {
        $input.val('');
      }
      
      $hint.removeClass('hide').addClass($input.val() == '' ? 'with-check' : 'hide');
      
      data.push({
        currentVal: element,
        solutionVal: solution[index],
        rowNum: $input.data('rownum'),
        colNum: $input.data('colnum'),
        gridNum: $input.data('gridnum')
      })
    });

    // DOM stuff to pass to the model
    sudokuData.injectDataAttributes(data);
  }
  
  function handleBoardLoadError () {
    // TODO: Update UI to show error.
  }
  
  function handleBoardSolved () {
    $statusMessage.text('Board solved!');
  }
  
  function handleBoardNotSolved () { }
  
  function handleCurrentValChanged (data, event) {
    var $input = getInputAtIndex(data.index),
        board = sudokuData.getBoard(),
        solution = sudokuData.getSolution();

    $statusMessage.text('Answer filled: ' + data.val);
    
    // If the input matches the correct solve val, highlight the cell in green, and hide the hint button
    // Delay this by amount from the event, since sometimes we need to let the animation happen
    setTimeout(function () {
      $input.val(data.val);
      if ($input.val() == solution[data.index]) {
        var $hint = $input.prev('.board-cell--hint');
        $hint.addClass('hide');
        $input.css({'background-color': '#11EE44'});
        TweenMax.to($input, 0.5, {backgroundColor: '#FFFFFF', ease:Quad.easeOut});
      } else {
        $input.css({'background-color': '#EE1144'});
        TweenMax.to($input, 0.5, {backgroundColor: '#FFFFFF', ease:Quad.easeOut});
      }
    }, data.delay);
  }
  
  function handleCellFocus (event) {
    // Store the focused cell
    $focusedCell = $(this);
    // parseInt is the fastest according to jsperf.com
    var cellnum    = getIndexOfInput($(this)),
        possible   = sudokuData.getPossibleValsForCell(cellnum);
    
    $statusMessage.text('Possibilities: ' + possible.join(', '));
  }
  
  function handleCellInput (event) {
    var $this = $(this);
    var val = $this.val();
    if (!_.contains([1,2,3,4,5,6,7,8,9], parseInt(val))) {
      $this.val('');
      return;
    }
    // update _board array
    var index = getIndexOfInput($this);
    sudokuData.updateValAtIndex(val, index);
    sudokuData.checkBoardForWin();
  }
  
  function handleKeyUp (event) {
    // only respond if a cell has focus
    if($boardnums.is(':focus')) {
      var cellnum = getIndexOfInput($focusedCell),
          newFocus = 0,
          which = event.which,
          arrows = [37,38,39,40];
      
      switch(which) {
        case arrows[1]: // up
          newFocus = Math.max(0, cellnum - 9);
          break;

        case arrows[3]: // down
          newFocus = Math.min(80, cellnum + 9);
          break;

        case  arrows[0]: // left
          newFocus = Math.max(0, cellnum - 1);
          break;
        case arrows[2]: // right
          newFocus = Math.min(80, cellnum + 1);
          break;
      }
      
      if (arrows.indexOf(which) !== -1) {
        var $nextInput = $board.find('#c' + newFocus);
        $nextInput.focus();
      }
    }
  }
  
  function handleResize (event) {
    // The board width is 70% of the window width, but not if that makes it taller than
    // the window height minus the header height.
    // The board height is never more than the window height minus the header height.
    var innerHeight = window.innerHeight,
        maxBoardHeight = Math.min(innerHeight - headerHeight - 60, window.innerWidth * 0.7),
        targetCellSize = parseInt((maxBoardHeight - (borderWidth * 4)) / 9);
    $board.find('.board-cell--input').css({'width': targetCellSize + 'px', 'height': targetCellSize + 'px'});
  }
  
  function handleHintClicked (event, $targ) {
    event.preventDefault();
    var $back = $targ.parent().find('.board-cell--back'),
        $input = $targ.parent().find('input'),
        index = getIndexOfInput($input),
        solutionVal = sudokuData.getValAtIndex(index).solutionVal;
    
    $targ.addClass('hide');
    // move the answer into view
    TweenMax.to($back.eq(0), 0.3, {y:0, ease: Quad.easeIn});
    
    // Update the model
    sudokuData.updateValAtIndex(solutionVal, index, 500);
    sudokuData.checkBoardForWin();
  }
  
  function handleNewGameClick (event) {
    $statusMessage.text('Loading new game...');
    sudokuData.loadBoard();
  }
  
  function handleSolveClick (event) {
    var board = sudokuData.getSolution();
    _.each(board, function (element, index) {
      sudokuData.updateValAtIndex(element, index, 0);
    });
    sudokuData.checkBoardForWin();
  }
  
  
  
  /*
   * Returns the integer-based index of the input cell
   * @param {jquery} $input: A jQuery selector of the input text field
   * @returns {Number} The 0-based index of that input
   */
  function getIndexOfInput($input) {
    return parseInt($input.attr('id').substring(1));
  }
  
  function getInputAtIndex (index) {
    return $board.find('input#c'+index).eq(0);
  }
  
  
  
  return {
    init         :init,
    zoomOut      :zoomOut,
    loadPuzzle   :loadPuzzle
  }
  
});