### Sudoku Game

This is a Sudoku game written in HTML, CSS and JavaScript. It's targeted for modern browsers, so please view it from modern Chrome, FireFox or Safari. The technology is split into two parts -- the front end of the site, and an API which returns valid puzzles and their solutions. This is the front end part. The back end part is located on Bitbucket [here](https://bitbucket.org/llague/sudokupuzzle-backend), and you can see the JSON output [here](http://nameless-woodland-1994.herokuapp.com/api/puzzle). A running example of the puzzle is viewable [here](http://sudokufuntest.herokuapp.com/).

To build the project, [Grunt](http://gruntjs.com/) task runner is used. Once you have the code downloaded:
1. Run `npm install` (assuming you have Node installed. I'm using v0.10.28)
2. Run `bower install` (assuming you have Bower installed)
2. Run the `grunt` command from the project root. The default Grunt task creates a local server, compiles SASS via Compass, autoprefixes CSS, [assembles](http://assemble.io/) static pages from [Swig](http://paularmstrong.github.io/swig/) templates, and watches files for any changes. I purpoasely didn't add a minification step, since this code is meant to be a demonstration, and minification would not make code inspection as easy.

The code is structured to be modular, with two main classes handling the actual Sudoku logic. These files are sudoku_data.js and sudoku_ui.js. Sudoku_data.js handles the model end of the board, including storing board state, loading new boards from the API, and other gameplay functionality like numbers in a selected grid, column or row. Sudoku_ui.js handles any UI-related tasks, such as responding to keyboard events, handling numbers added to the board, handling click events on buttons, etc.
 
 To handle the initial reveal that happens on the page, there is panel_revealer.js. This file exposes open() and close() methods to open and close the revealer, and most of the code is to handle click events on buttons on handle the open/close animations.
 
 JavaScript loading is done with Require.js, so the main file to get loaded on the initial page load is config.js. This file handles require.js configuration, and then loads up the main.js file, which is the main controller of gameplay. Main.js initializes sudoku_ui.js, sudoku_data.js and panel_revealer.js, and handles event communication between them. 
 
 Event communication is done with [Postal.js](https://github.com/postaljs/postal.js), which is a pub/sub library for JavaScript. This message bus system allows messages to be sent between modules without those modules knowing about each other. This allows for more modular code, and reduces interdependencies.