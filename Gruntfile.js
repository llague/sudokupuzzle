module.exports = function (grunt) {

  var pretty = require('pretty');
  
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
      js: {
        files: ['app/js/{,*/}*.js'],
        tasks: ['jshint'],
        options: {
          livereload: true
        }
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      sass: {
        files: ['app/scss/{,*/}*.scss', 'lib/swig_extensions/**/*.js'],
        tasks: ['compass', 'autoprefixer']
      },
      assemble: {
        files: ['app/templates/**/*.swig'],
        tasks: ['assemble']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          'app/{,*/}*.html',
          'app/images/{,*/}*'
        ]
      }
    },

    connect: {
      options: {
        port: 9000,
        open: true,
        livereload: 35729,
        hostname: 'localhost'
      },
      livereload: {
        options: {
          middleware: function (connect) {
            return [
              connect.static('app'),
              connect().use('app/bower_components', connect.static('./app/bower_components'))
            ];
          }
        }
      },
      dist: {
        options: {
          base: 'app',
          livereload: false
        }
      }
    },

    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        eqnull: true,
        browser: true,
        globals: {
          jQuery: true
        }
      }
    },

    compass: {
      options: {
        sassDir: 'app/scss',
        cssDir: 'app/css',
        generatedImagesDir: 'app/images/generated',
        imagesDir: 'app/images',
        javascriptsDir: 'app/js',
        fontsDir: 'app/fonts',
        importPath: 'app/bower_components',
        httpImagesPath: '/images',
        httpGeneratedImagesPath: '/images/generated',
        httpFontsPath: '/fonts',
        relativeAssets: false
      },
      dist: {
        options: {
          generatedImagesDir: 'app/images/generated'
        }
      },
      server: {
        options: {
          debugInfo: true
        }
      }
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      dist: {
        files: [
          {
            expand: true,
            cwd: 'app/css/',
            src: '{,*/}*.css',
            dest: 'app/css/'
          }
        ]
      }
    },

    assemble: {
      options: {
        engine: 'swig',
        data: ['app/data/*.json'],
        partials: ['app/templates/partials/*.swig'],
        layoutdir: 'app/templates/layouts',
        layoutext: '.swig',
        layout: 'default',
        flatten: true,
        postprocess: pretty,
        root: 'app',
        helpers: ['lib/swig_extensions/filters/*.js']
      },
      pages: {
        options: {
          data:'app/data/homepage.json'
        },
        src: ['app/templates/pages/*.swig'],
        dest: 'app/'
      }
    }
  });
  
  grunt.loadNpmTasks('assemble');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-jshint');

  grunt.registerTask('serve', function (target) {
    grunt.task.run([
      'autoprefixer',
      'connect:livereload',
      'watch'
    ]);
  });
  
  grunt.registerTask('default', ['assemble', 'compass', 'serve']);
  
};