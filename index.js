var stat = require('node-static');

var file = new stat.Server('./app'),
    port = process.env.PORT || 9000;

require('http').createServer(function (request, response) {
  request.addListener('end', function () {
    file.serve(request, response);
  }).resume();
}).listen(port, '0.0.0.0');
console.log('Server started on http://localhost:' + port);