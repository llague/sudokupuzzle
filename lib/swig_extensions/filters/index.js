
var filters = module.exports = {};

filters.mod = function (num, mod) {
  return num % mod === 0;
};

/*
 * Returns the grid number of that cell.
 * A single grid is 3x3, and there are 3x3 grids.
 * So cell in col 0, row 0 is in grid 0.
 * Cell col 3, row 0 is in grid 1
 */
filters.gridnum = function (cellnum, rownum) {
  return Math.floor(cellnum / 3) + (Math.floor(rownum / 3) * 3);
};

filters.getBorderClass = function (cellnum, rownum) {
  var classString = '';
  if (rownum % 3 == 0) {
    classString += 't';
  }
  if (cellnum == 0) {
    classString += ' l';
  }
  if (cellnum % 3 == 0 && cellnum != 0) {
    classString += ' l';
  }
  if (cellnum == 8) {
    classString += ' r';
  }
  if (rownum == 8) {
    classString += ' b';
  }
  return classString;
};

filters.getCellId = function (cellnum, rownum) {
  return 'c' + (cellnum + rownum * 9);
};

module.exports.register = function (swig, opts) {
  opts = opts || {};

  for (var filter in filters) {
    if (filters.hasOwnProperty(filter)) {
      swig.setFilter(filter, filters[filter]);
    }
  }
};
